﻿using ImageResizeWebApp.Helpers;
using ImageResizeWebApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ImageResizeWebApp.Controllers
{
	[Route("api/[controller]")]
    public class ImagesController : Controller
    {
        // make sure that appsettings.json is filled with the necessary details of the azure storage
        private readonly AzureStorageConfig storageConfig = null;

        public ImagesController(IOptions<AzureStorageConfig> config)
        {
            storageConfig = config.Value;
        }

		// POST /api/images/uploadtostorage
        [HttpPost("uploadtostorage")]
        public async Task<IActionResult> UploadToStorage(ICollection<IFormFile> files, string containerName, string accountName, string accountKey)
        {
            string uploadedFileUri = "";

            try
            {
                if (files.Count == 0)
                    return BadRequest("No files received from the upload");
	
				long fileSize = 0;

                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                        using (Stream stream = formFile.OpenReadStream())
                        {
                            uploadedFileUri = await StorageHelper.UploadToStorage(stream, formFile.FileName, containerName, accountName, accountKey);
							fileSize = stream.Length;
						}
                    }
                }

                if (!string.IsNullOrEmpty(uploadedFileUri))
                {
					return new AcceptedAtActionResult("GetThumbNails", "Images", null, new { 
						uploadedFileUri,
						fileSize
					});
                }
                else
                    return BadRequest("Look like the image couldnt upload to the storage");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST /api/images/upload
        [HttpPost("[action]")]
        public async Task<IActionResult> Upload(ICollection<IFormFile> files)
        {
            bool isUploaded = false;

            try
            {
                if (files.Count == 0)
                    return BadRequest("No files received from the upload");

                 if (storageConfig.AccountKey == string.Empty || storageConfig.AccountName == string.Empty)
                    return BadRequest("sorry, can't retrieve your azure storage details from appsettings.js, make sure that you add azure storage details there");

                if (storageConfig.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in the azure blob storage");

                foreach (var formFile in files)
                {
                    if (StorageHelper.IsImage(formFile))
                    {
                        if (formFile.Length > 0)
                        {
                            using (Stream stream = formFile.OpenReadStream())
                            {
                                isUploaded = await StorageHelper.UploadFileToStorage(stream, formFile.FileName, storageConfig);
                            }
                        }
                    }
                    else
                    {
                        return new UnsupportedMediaTypeResult();
                    }
                }

                if (isUploaded)
                {
                    if (storageConfig.ThumbnailContainer != string.Empty)
                        return new AcceptedAtActionResult("GetThumbNails", "Images", null, null);
                    else
                        return new AcceptedResult();
                }
                else
                    return BadRequest("Look like the image couldnt upload to the storage");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


		// GET ("containers")]
		[HttpGet("containers")]
        public async Task<IActionResult> Containers()
        {

            try
            {
            
                 if (storageConfig.AccountKey == string.Empty || storageConfig.AccountName == string.Empty)
                    return BadRequest("sorry, can't retrieve your azure storage details from appsettings.js, make sure that you add azure storage details there");

                if (storageConfig.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in the azure blob storage");

				return new ObjectResult(await StorageHelper.GetContainersList(storageConfig));               
                       
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


		// POST("createcontainer")]
		[HttpPost("createcontainer")]
        public async Task<IActionResult> CrateContainer(string containerName)
        {
            try
            {
				return new ObjectResult(await StorageHelper.CreateContainer(containerName, storageConfig));               
                       
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


		// GET("blobList")]
		[HttpPost("bloblist")]
        public async Task<IActionResult> BlobList(string containerName)
        {
            try
            {
				return new ObjectResult(await StorageHelper.GetBlobList(storageConfig, containerName));               
                       
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET /api/images/thumbnails
        [HttpGet("thumbnails")]
        public async Task<IActionResult> GetThumbNails()
        {
            try
            {
                if (storageConfig.AccountKey == string.Empty || storageConfig.AccountName == string.Empty)
                    return BadRequest("Sorry, can't retrieve your Azure storage details from appsettings.js, make sure that you add Azure storage details there.");

                if (storageConfig.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in Azure blob storage.");

                List<string> thumbnailUrls = await StorageHelper.GetThumbNailUrls(storageConfig);
                return new ObjectResult(thumbnailUrls);            
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

		// GET /api/images/imageUrls
        [HttpGet("imageUrls")]
        public async Task<IActionResult> GetImageUrls()
        {
            try
            {
                if (storageConfig.AccountKey == string.Empty || storageConfig.AccountName == string.Empty)
                    return BadRequest("Sorry, can't retrieve your Azure storage details from appsettings.js, make sure that you add Azure storage details there.");

                if (storageConfig.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in Azure blob storage.");

                List<string> imageUrls = await StorageHelper.GetImageUrls(storageConfig);
                return new ObjectResult(imageUrls);            
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}