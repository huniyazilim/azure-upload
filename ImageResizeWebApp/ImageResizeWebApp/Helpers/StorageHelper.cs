﻿using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using ImageResizeWebApp.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImageResizeWebApp.Helpers
{
    public static class StorageHelper
    {

        public static bool IsImage(IFormFile file)
        {

			// return true;	

            if (file.ContentType.Contains("image"))
            {
                return true;
            }

            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg", ".pdf", ".epub" };

            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

		public static async Task<string> UploadToStorage(Stream fileStream, string fileName, string containerName, 
															string accountName, string accountKey)
        {
         
			Uri blobUri = new Uri("https://" + accountName + ".blob.core.windows.net/" + containerName + "/" + fileName);

            // Create StorageSharedKeyCredentials object by reading
            StorageSharedKeyCredential storageCredentials =
                new StorageSharedKeyCredential(accountName, accountKey);

            // Create the blob client.
            BlobClient blobClient = new BlobClient(blobUri, storageCredentials);

            // Upload the file
            await blobClient.UploadAsync(fileStream);

            return await Task.FromResult(blobUri.ToString());
        }

        public static async Task<bool> UploadFileToStorage(Stream fileStream, string fileName, AzureStorageConfig _storageConfig)
        {
            // Create a URI to the blob
            Uri blobUri = new Uri("https://" +
                                  _storageConfig.AccountName +
                                  ".blob.core.windows.net/" +
                                  _storageConfig.ImageContainer +
                                  "/" + fileName);

            // Create StorageSharedKeyCredentials object by reading
            // the values from the configuration (appsettings.json)
            StorageSharedKeyCredential storageCredentials =
                new StorageSharedKeyCredential(_storageConfig.AccountName, _storageConfig.AccountKey);

            // Create the blob client.
            BlobClient blobClient = new BlobClient(blobUri, storageCredentials);

            // Upload the file
            await blobClient.UploadAsync(fileStream);

            return await Task.FromResult(true);
        }

        public static async Task<List<string>> GetThumbNailUrls(AzureStorageConfig _storageConfig)
        {
            List<string> thumbnailUrls = new List<string>();

            // Create a URI to the storage account
            Uri accountUri = new Uri("https://" + _storageConfig.AccountName + ".blob.core.windows.net/");

            // Create BlobServiceClient from the account URI
            BlobServiceClient blobServiceClient = new BlobServiceClient(accountUri);

            // Get reference to the container
            BlobContainerClient container = blobServiceClient.GetBlobContainerClient(_storageConfig.ThumbnailContainer);

            if (container.Exists())
            {
                foreach (BlobItem blobItem in container.GetBlobs())
                {
                    thumbnailUrls.Add(container.Uri + "/" + blobItem.Name);
                }
            }

            return await Task.FromResult(thumbnailUrls);
        }

		public static async Task<List<string>> GetImageUrls(AzureStorageConfig _storageConfig)
        {
            List<string> imageUrls = new List<string>();

            // Create a URI to the storage account
            Uri accountUri = new Uri("https://" + _storageConfig.AccountName + ".blob.core.windows.net/");

            // Create BlobServiceClient from the account URI
            BlobServiceClient blobServiceClient = new BlobServiceClient(accountUri);

            // Get reference to the container
            BlobContainerClient container = blobServiceClient.GetBlobContainerClient(_storageConfig.ImageContainer);

            if (container.Exists())
            {
                foreach (BlobItem blobItem in container.GetBlobs())
                {
                    imageUrls.Add(container.Uri + "/" + blobItem.Name);
                }
            }

            return await Task.FromResult(imageUrls);
        }

		public static async Task<List<BlobItem>> GetBlobList(AzureStorageConfig _storageConfig, string containerName)
        {
            List<string> thumbnailUrls = new List<string>();

            // Create a URI to the storage account
            Uri accountUri = new Uri("https://" + _storageConfig.AccountName + ".blob.core.windows.net/");

            // Create BlobServiceClient from the account URI
            BlobServiceClient blobServiceClient = new BlobServiceClient(accountUri);

            // Get reference to the container
            BlobContainerClient container = blobServiceClient.GetBlobContainerClient(containerName);

			List<BlobItem> blobItems = new List<BlobItem>();

            if (container.Exists())
            {
                foreach (BlobItem blobItem in container.GetBlobs())
                {
                    blobItems.Add(blobItem);
                }
            }

            return await Task.FromResult(blobItems);
        }

		public static async Task<List<string>> GetContainersList(AzureStorageConfig _storageConfig) {
			StorageSharedKeyCredential storageCredentials =
                new StorageSharedKeyCredential(_storageConfig.AccountName, _storageConfig.AccountKey);
							
			Uri accountUri = new Uri("https://" + _storageConfig.AccountName + ".blob.core.windows.net/");

			BlobServiceClient client = new BlobServiceClient(accountUri, storageCredentials);

			var containers = client.GetBlobContainers();	

			List<string> names = new List<string>();

            foreach (var container in containers)
            {
                names.Add(container.Name);
            }

			return await Task.FromResult(names);
		}

		public static async Task<string> CreateContainer(string containerName, AzureStorageConfig _storageConfig) {
			StorageSharedKeyCredential storageCredentials =
                new StorageSharedKeyCredential(_storageConfig.AccountName, _storageConfig.AccountKey);
							
			Uri accountUri = new Uri("https://" + _storageConfig.AccountName + ".blob.core.windows.net/");

			BlobServiceClient client = new BlobServiceClient(accountUri, storageCredentials);

			var newContainer = await client.CreateBlobContainerAsync(containerName, PublicAccessType.BlobContainer);

			return await Task.FromResult(newContainer.Value.Uri.ToString());
		}
    
	}
}
